// YouTube modal
(function () {
  console.log("Hello GIT 2222")
  let open_modal_btn = document.querySelectorAll(".open-modal-btn");

  open_modal_btn.forEach(btn => {
    btn.onclick = function () {
      let modal_id = btn.dataset.openModal
      let modal = document.querySelector("#" + modal_id)

      modal.classList.add("modal--show");
      modal_close = modal.querySelectorAll(".modal__close")

      modal_close.forEach(close => {
        close.onclick = function () {

          modal.classList.remove("modal--show")
          let player = modal.querySelectorAll("iframe")
          player[0].attributes.src.value = player[0].attributes.src.value

        }
      })

    }
  })
})();

// carousel
window.onload = function () {
  let options_card = {
    cellAlign: 'center',
    contain: true,
    wrapAround: true
  }
  
  if ( matchMedia('screen and (max-width: 1140px)').matches ) {
    options_card.prevNextButtons = false;
    options_card.wrapAround = false
  }

  var flkty = new Flickity( '.question_carousel', options_card);

  let options_img = {
    cellAlign: 'center',
    wrapAround: false
  }

  var flkty = new Flickity( '.card__carousel', options_img);
}

// $(document).ready(function () {
//   $('.question_carousel').slick({
//     infinite: true,
//     slidesToShow: 1,
//     dots: false,
//     // centerMode: true,
//     centerPadding: '60px',
//     nextArrow: '<img class="slick-btn slick-next" src="../img/next.png">',
//     prevArrow: '<img class="slick-btn slick-prev" src="../img/next.png">',
//     responsive: [
//       {
//         breakpoint: 1140,
//         settings: {
//           arrows: false,
//           // centerMode: true,
//           centerPadding: '40px',
//           slidesToShow: 1
//         }
//       },
//     ],
//   });
// });